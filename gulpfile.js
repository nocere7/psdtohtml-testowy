var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

var staticPath = 'public/static';
//expanded or compressed
var cssCompressed = {outputStyle: 'expanded'};
var jsCompressed = false;

//tasks
gulp.task('build-sass', buildSass);
gulp.task('watch-sass', watchSass);
gulp.task('build-js', buildJs);
gulp.task('watch-js', watchJs);

//functions
function buildSass() {
    gulp.src('src/scss/main.scss')
        .pipe(sass({includePaths: ['node_modules/minimal-css/src/scss']}))
        .pipe(sass(cssCompressed).on('error', sass.logError))
        .pipe(concat('app.css'))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(staticPath + '/'))
}

function watchSass() {
    gulp.watch('src/scss/**/*.scss', ['build-sass']);
}

function buildJs() {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'src/js/*.js',
        'src/js/*/*.js',
        'src/js/*/*/*.js',
        'src/js/*/*/*/*.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(gulpif(jsCompressed, uglify({
            mangle: false
        })))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(staticPath + '/'));
}

function watchJs() {
    gulp.watch('src/js/**/*.js', ['build-js']);
}