function App(options) {

    var self = this;
    self.options = options;

    self.options.menuToggleElement.click({
        windowClass: self.options.menuToggleWindowClass
    }, toggleMenu);

    self.options.pageBody.scroll({
        headerElement: self.options.headerElement,
        headerScrollClass: self.options.headerScrollClass
    }, scrollMenu);

    self.options.sliders.forEach(function (el) {
        console.log(el);
        slider(el);
    });

    $('[' + self.options.scrollToAttr + ']').click({
        scrollToAttr: self.options.scrollToAttr,
        scrollTime: self.options.scrollTime
    }, scrollTo);
}

$(document).ready(function () {
    var app = new App({
        menuToggleElement: $('[data-menu-toggle]'),
        menuToggleWindowClass: 'open-menu',
        pageBody: $(window),
        headerElement: $('#page-inner').find('> header'),
        headerScrollClass: 'scroll',
        sliders: [
            {
                body: '#slider-1',
                container: '.slider-container',
                slide: '.slide',
                slidesPerContainer: 3,
                paginationContainer: '.pagination',
                paginationCurrentClassName: 'current'
            },
            {
                body: '#slider-2',
                container: '.slider-container',
                slide: '.slide-row',
                slidesPerContainer: 1,
                paginationContainer: '.pagination',
                paginationCurrentClassName: 'current'
            }
        ],
        scrollToAttr: 'data-scroll-to',
        scrollTime: 700
    });

    var y, upElement = $('#up');
    $(window).scroll(function (e) {
        y = $(this).scrollTop();

        if( y > 0) {
            upElement.addClass('show');
        }
        else {
            upElement.removeClass('show');
        }
    });
});