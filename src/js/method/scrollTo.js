function scrollTo(event) {

    var scrollTo = $(this).attr(event.data.scrollToAttr);

    $("html, body").animate({scrollTop: $(scrollTo).offset().top - 55}, event.data.scrollTime);

}