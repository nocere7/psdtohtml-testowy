function slider(el) {

    var currentIndex = 0;
    var shift = 0;
    var body = $(el.body);
    var countSlides = body.find(el.slide).length;
    var countContainers = countSlides / el.slidesPerContainer;

    for (var i = 0; i < countContainers; i++) {
        body.find(el.paginationContainer).append('<div>' + (i +1) + '</div>')
    }

    body.find(el.paginationContainer).find('div:first-child').addClass(el.paginationCurrentClassName);

    body.find(el.paginationContainer).find('div').click(function () {
        currentIndex = body.find(el.paginationContainer).find('div').index(this);
        shiftSlider();
    });

    body.find('.arrow-left').click(function () {
        currentIndex = currentIndex - 1;
        if(currentIndex < 0) {
            currentIndex = 0;
        }
        shiftSlider();
    });

    body.find('.arrow-right').click(function () {
        currentIndex = currentIndex + 1;
        if(currentIndex > countContainers - 1) {
            currentIndex = countContainers - 1;
        }
        shiftSlider();
    });

    function shiftSlider() {
        body.find(el.paginationContainer).find('div').removeClass(el.paginationCurrentClassName);
        body.find(el.paginationContainer).find('div:eq( ' + currentIndex + ' )').addClass(el.paginationCurrentClassName);

        shift = -1 * currentIndex * 100;

        body.find(el.container).css({
            '-ms-transform': 'translateX(' + shift + '%)',
            '-webkit-transform': 'translateX(' + shift + '%)',
            'transform': 'translateX(' + shift + '%)'
        })
    }

}