function scrollMenu(event) {

    if($(this).scrollTop() > 0) {
        event.data.headerElement.addClass(event.data.headerScrollClass);
    }
    else {
        event.data.headerElement.removeClass(event.data.headerScrollClass);
    }

}